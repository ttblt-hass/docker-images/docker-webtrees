FROM php:8.4.4-apache

ENV PATH="${PATH:-/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin}"

ENV WEBTREES_VERSION=2.1.20

#RUN apt-get update && apt-get install -y wget unzip apache2 libapache2-mod-php php-mysql php-gd mysql-client && rm -rf /var/lib/apt/lists/* && apt-get clean
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libzip-dev \
	zlib1g-dev \
	libicu-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install -j$(nproc) pdo_mysql \
    && docker-php-ext-install -j$(nproc) zip \
    && docker-php-ext-install -j$(nproc) exif \
    && docker-php-ext-configure intl \
    && docker-php-ext-install -j$(nproc) intl \
    && apt-get remove -y --purge \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libzip-dev \
	zlib1g-dev \
	libicu-dev \
    && apt-get clean

WORKDIR /src
RUN apt-get update && apt-get install -y unzip wget \
	&& wget https://github.com/fisharebest/webtrees/releases/download/${WEBTREES_VERSION}/webtrees-${WEBTREES_VERSION}.zip \
	&& unzip webtrees-${WEBTREES_VERSION}.zip \
	&& mv webtrees /var/www/ \
	&& rm -rf webtrees-${WEBTREES_VERSION}.zip \
	&& apt-get remove -y --purge wget unzip \
	&& apt-get clean

WORKDIR /var/www/webtrees

ENV APACHE_DOCUMENT_ROOT /var/www/webtrees

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

#RUN echo '<VirtualHost *:80>' > /etc/apache2/conf-enabled/webtrees.conf
#RUN echo '  DocumentRoot "/var/www/webtrees"' >> /etc/apache2/conf-enabled/webtrees.conf
#RUN echo '</VirtualHost>' >> /etc/apache2/conf-enabled/webtrees.conf

RUN mkdir -p /var/www/webtrees/data
RUN chown www-data:www-data -R /var/www/webtrees/data

ENV APACHE_RUN_USER=www-data
ENV APACHE_PID_FILE=/var/run/apache2.pid
ENV APACHE_RUN_GROUP=www-data
ENV APACHE_RUN_DIR=/var/run/apache2
ENV APACHE_LOG_DIR=/var/log/apache2
#ADD startup.sh /usr/local/bin/startup.sh
#RUN chmod +x /usr/local/bin/startup.sh

EXPOSE 80 443

#CMD /usr/local/bin/startup.sh
